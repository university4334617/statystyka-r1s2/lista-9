# Wczytanie danych 
data <- na.omit(read.csv("C:/Users/Weronika/Desktop/IT/Statystyka_S2/L9/waga1.csv", head=TRUE, sep=";"))

# Test Z
# Wyznaczenie wartości p
Z_test <- function(data, mu){
  n <- length(data)
  x_bar <- mean(data)
  sigma <- sd(data)
  Z <- (x_bar - mu) / (sigma / sqrt(n))
  p_value <- 2 * pnorm(-abs(Z))  # Dwustronny test
  return(p_value)
}

# Test Studenta (t-test)
t_test_statistic <- function(data, mu){
  n <- length(data)
  x_bar <- mean(data)
  sigma <- sd(data)
  t <- (x_bar - mu) / (sigma / sqrt(n))
  return(t)
}

wzrost <- data$Wzrost[data$plec == 0]
t_test <- t.test(wzros, mu = 172)


cat("Test Z:\n")
cat("Wartość p:", Z_test(wzros, 172), "\n")
cat("Test Studenta (t-test):\n")
cat("Wartość statystyki testowej:", t_test_statistic(wzros, 172), "\n")
cat("Wartość p:", t_test$p.value, "\n")
cat("Wynik testu:", ifelse(t_test$p.value < 0.05, "Odrzucamy H_0", "Nie odrzucamy H_0"), "\n")
